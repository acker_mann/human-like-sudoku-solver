import sys


def same_row(i, j):
    """true iff same row indices.
    e.g. 18 // 9 =  2    = 24//9"""
    return i//9 == j//9

def same_col(i, j):
    """true iff same col indices.
    e.g. 13 % 9 =   4   = 22 % 9
    """
    return i % 9 == j % 9

def same_box(i, j):
    """true iff in the same box.
    
    first line is true iff same third of rows
    second line is true iff same third of cols
    """
    return ( i // 27 ) == ( j // 27 ) and \
            ( i // 3 ) % 3 == ( j // 3 ) % 3 


def paste_puzzle(puzzle):
    for index, field in enumerate(puzzle):
        if index % 27 == 0: print("-"*(3*3*3+4))
        if index % 3 == 0: print("|", end="")
        if type(field) == int:
            print(" "+str(field), end=" ")
        else:
            if field.count(True) > 3:
                print(" @", end=" ")
            elif field.count(True) == 3:
                print(" ³", end=" ")
            elif field.count(True) == 2:
                print(" ²", end=" ")
            else:
                print(" !", end=" ")
            #print("  ", end=" ")
        
        if index % 9 == 8: print("|")
    print("-"*(3*3*3+4))


def remove_fixed(puzzle):
    for x in range(81):
        for y in range(81):
            if same_row(x,y) or same_col(x,y) or same_box(x,y):
                if type(puzzle[x]) == int:
                    if type(puzzle[y]) != int:
                   #     print(x+1, ' sets ',y+1,' to ', end='')
                        puzzle[y][puzzle[x]-1] = False
                    #    print(puzzle[y])
                elif type(puzzle[y]) == int:
                    puzzle[x][puzzle[y]-1] = False

def is_cleared(bool_array):

    if not True in bool_array:
        raise ValueError('no option left')

    if (bool_array.count(True) == 1):
        return bool_array.index(True)

    return -1
            

def set_if_clear(puzzle):
    for x in range(81):
        if type(puzzle[x]) != int:
            val = is_cleared(puzzle[x])
            if val >= 0:
                puzzle[x] = val

#def set_if_last_hope(puzzle):
#    for looking_for in range(1,10):


def start_solving(unsolved_puzzle):

    print("Starting solving")
#    puzzle = [[True for x in range(9)]]*81 -- argh -.-
    puzzle = [[]]*81
    for index, x in enumerate(unsolved_puzzle):
        try:
            number = int(x)
            puzzle[index] = number
        except ValueError:
            puzzle[index] = [True for x in range(9)]

    print("input: ")
    paste_puzzle(puzzle)

    while True:
        remove_fixed(puzzle)

        set_if_clear(puzzle)

#TODO        set_if_last_hope(puzzle)

        print("sofar: ")
        paste_puzzle(puzzle)
        print(puzzle[60])
        input("Press Enter to continue...")



if __name__ == '__main__':
    if len(sys.argv) == 2 and len(sys.argv[1]) == 81:
        start_solving(sys.argv[1])
    else:
        sample_puzzle='.3.6.5...6...9...2.7.1....6.9.......81..5..69.......8.4....3.2.9...2...5...9.8.3.'
        print(" ### SOLVING SAMPLE ###")
        start_solving(sample_puzzle)
        
"""print('Usage: python solver.py puzzle')
print('  where puzzle is an 81 character string')
print('  representing the puzzle read left-to-right,')
print("  top-to-bottom, and '.' is a blank")
"""
